<?php

namespace Drupal\leaflet_geojson_file;

use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\Component\Utility\Html;
use Drupal\leaflet\LeafletService;
use Drupal\node\NodeInterface;

/**
 * Provides a LeafletService class.
 */
class LeafletGeoJsonService extends LeafletService {

  /**
   * Load all Leaflet required client files and return markup for a map.
   *
   * @param array $map
   *   The map settings array.
   * @param array $features
   *   The features array.
   * @param string $height
   *   The height value string.
   *
   * @return array
   *   The leaflet_map render array.
   */
  public function leafletRenderMap(array $map, array $features = [], $height = '400px') {
    $map_id = isset($map['id']) ? $map['id'] : Html::getUniqueId('leaflet_map');
    $attached_libraries = ['leaflet_geojson_file/leaflet-geojson', 'leaflet_geojson_file/leaflet.ajax', 'leaflet/general'];
    // Add the Leaflet Fullscreen library, if requested.
    if (isset($map['settings']['fullscreen_control'])) {
      $attached_libraries[] = 'leaflet/leaflet.fullscreen';
    }

    $settings[$map_id] = [
      'mapid' => $map_id,
      'map' => $map,
      // JS only works with arrays, make sure we have one with numeric keys.
      'features' => array_values($features),
    ];
    return [
      '#theme' => 'leaflet_map',
      '#map_id' => $map_id,
      '#height' => $height,
      '#map' => $map,
      '#attached' => [
        'library' => $attached_libraries,
        'drupalSettings' => [
          'geojsonleaflet' => $settings,
        ],
      ],
    ];
  }

  /**
   * Returns the relative url of a file.
   *
   * @param $fid
   *
   * @return string
   */
  public function leafletProcessGeofieldFileUrl($fid, NodeInterface $entity) {
    /** @var \Drupal\file\Entity\File $file */
    $file = File::load($fid);
    if ($file) {
      /** @var \Drupal\Core\Url $file_uri */
      $file_uri = Url::fromUri($file->url());
      $file_uri->setOption('query', ['v' => $entity->getRevisionId()]);
      return file_url_transform_relative($file_uri->toUriString());

    }
  }

}
