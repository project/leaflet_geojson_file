<?php

namespace Drupal\leaflet_geojson_file\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\leaflet\LeafletService;
use Drupal\leaflet_widget\LeafletWidgetSettingsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the "leaflet_widget" widget.
 *
 * @FieldWidget(
 *   id = "geojson_leaflet_widget",
 *   label = @Translation("Leaflet GeoJSON Map Widget"),
 *   description = @Translation("Provides a map powered by Leaflet and Leaflet.widget for a file field."),
 *   field_types = {
 *     "file",
 *   },
 * )
 */
class LeafletGeoJsonWidget extends FileWidget {

  use LeafletWidgetSettingsTrait;
  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  private $widget_plugin_manager;

  /**
   * @var \Drupal\leaflet\LeafletService
   */
  private $leafletService;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $file_system;

  /**
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  private $stream_wrapper_manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ElementInfoManagerInterface $element_info,
    WidgetPluginManager $widget_plugin_manager,
    LeafletService $leaflet_service,
    FileSystemInterface $file_system,
    StreamWrapperManager $stream_wrapper_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info);
    $this->widget_plugin_manager = $widget_plugin_manager;
    $this->leafletService = $leaflet_service;
    $this->file_system = $file_system;
    $this->stream_wrapper_manager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('plugin.manager.field.widget'),
      $container->get('leaflet.service'),
      $container->get('file_system'),
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    parent::settingsForm($form, $form_state);
    $form = $this->getWidgetSettingsForm($form);

    $form['input']['#title'] = $this->t('Input settings');
    $form['input']['show']['#title'] = $this->t('Show textarea element');
    $form['input']['show']['#readonly'] = $this->t('Make input element read-only');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#element_validate'][] = [get_class($this), 'validateJson'];

    $fid = $items[$delta]->target_id ?: NULL;
    $file = NULL;
    if (!empty($fid)) {
      /** @var \Drupal\file\Entity\File $file */
      $file = File::load($fid);
    }

    $json_element_name = 'leaflet-widget-input';

    $element['geojson_input'] = [
      '#title' => $this->t('Copy/paste GeoJSON data'),
      '#attributes' => ['class' => [$json_element_name]],
      '#type' => 'hidden',
      '#weight' => -10,
      '#rows' => 7,
      '#default_value' => $items[$delta]->value ?: NULL,
    ];

    $element['hint'] = [
      '#markup' => '<p><strong>' . $this->t('If you already have a GeoJSON file, you can upload it directly.') . '</strong></p>',
      '#weight' => -1,
    ];

    // We will only render a Leaflet map, if the GeoJson File is not big than 5MB.
    if ($file && $file->getSize() > 5000000) {
      return $element;
    }

    // Show or hide the input field based on the settings.
    if ($this->settings['input']['show'] === "1") {
      $element['geojson_input']['#type'] = 'textarea';
    }

    if ($file) {
      $json_data = file_get_contents($file->getFileUri());
      $element['geojson_input']['#default_value'] = $json_data;
    }

    // Determine map settings and add map element.
    $map_settings = $this->getSetting('map');
    $input_settings = $this->getSetting('input');
    $js_settings = [];
    $map = leaflet_map_get_info($map_settings['leaflet_map']);
    $map['settings']['center'] = $map_settings['center'];
    $map['settings']['zoom'] = $map_settings['zoom'];

    if (!empty($map_settings['locate'])) {
      $js_settings['locate'] = TRUE;
      unset($map['settings']['center']);
    }

    $element['map'] = $this->leafletService->leafletRenderMap($map, [], $map_settings['height'] . 'px');
    $element['map']['#weight'] = -20;

    // Build JS settings for leaflet widget.
    $js_settings['map_id'] = $element['map']['#map_id'];
    $js_settings['jsonElement'] = '.' . $json_element_name;
    $cardinality = $items->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    $js_settings['multiple'] = $cardinality == 1 ? FALSE : TRUE;
    $js_settings['cardinality'] = $cardinality > 0 ? $cardinality : 0;
    $js_settings['autoCenter'] = $map_settings['auto_center'];
    $js_settings['inputHidden'] = empty($input_settings['show']);
    $js_settings['inputReadonly'] = !empty($input_settings['readonly']);
    $js_settings['toolbarSettings'] = !empty($this->getSetting('toolbar')) ? $this->getSetting('toolbar') : [];
    $js_settings['scrollZoomEnabled'] = !empty($map_settings['scroll_zoom_enabled']) ? $map_settings['scroll_zoom_enabled'] : FALSE;
    $js_settings['fullscreenControl'] = !empty($map_settings['fullscreen_control']) ? $map_settings['fullscreen_control'] : TRUE;

    // Include javascript.
    $element['map']['#attached']['library'][] = 'leaflet_widget/widget';
    // Leaflet.draw plugin.
    $element['map']['#attached']['library'][] = 'leaflet_widget/leaflet-geoman';

    // Add fullscreen control if requested.
    if ($js_settings['fullscreenControl']) {
      $element['map']['#attached']['library'][] = 'leaflet/leaflet.fullscreen';
    }

    // Settings and geo-data are passed to the widget keyed by field id.
    $element['map']['#attached']['drupalSettings']['leaflet_widget'] = [$element['map']['#map_id'] => $js_settings];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateJson($element, FormStateInterface $form_state, $form) {

    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    $field_name = $element['#field_name'];

    if (!empty($value['geojson_input'])) {
      if (json_last_error() !== JSON_ERROR_NONE) {
        $form_state->setErrorByName($field_name, t('Invalid JSON data'));
      }
      $data = $value['geojson_input'];

      if (!empty($value['fids']) && !empty($value['fids']['0'])) {
        $fid = $value['fids']['0'];
        /** @var \Drupal\file\Entity\File $file */
        $file = File::load($fid);
        file_save_data($data, $file->getFileUri(), FileSystemInterface::EXISTS_REPLACE);
      }
      else {
        if ($form_state->getFormObject() instanceof EntityFormInterface) {
          /** @var \Drupal\Core\Entity\EntityInterface $entity */
          $entity = $form_state->getFormObject()->getEntity();

          /** @var \Drupal\field\Entity\FieldConfig $field_definition */
          $field_definition = $entity->getFieldDefinition($field_name);
          $field_storage = $field_definition->getFieldStorageDefinition();
          $destination_scheme_name = $field_storage->getSetting('uri_scheme');
          $directory = $field_definition->getSetting('file_directory');

          /** @var \Drupal\Core\File\FileSystem $file_system */
          $file_system = \Drupal::service('file_system');
          $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
          $stream_wrapper = $stream_wrapper_manager->getViaScheme($destination_scheme_name);
          $destination = $stream_wrapper->getUri() . $directory . '/' . $entity->uuid() . '-' . $field_name . '.json';
          $dirname = $file_system->dirname($destination);
          $file_system->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY);
          $file = file_save_data($value['geojson_input'], $destination, FileSystemInterface::EXISTS_REPLACE);
          if ($file) {
            $values['fids'][] = $file->id();
            NestedArray::setValue($form_state->getValues(), $element['#parents'], $values);
          }
        }
      }
    }
  }

}
