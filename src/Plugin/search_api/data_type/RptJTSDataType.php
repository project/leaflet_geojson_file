<?php

namespace Drupal\leaflet_geojson_file\Plugin\search_api\data_type;

use Drupal\file\Entity\File;
use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides the location data type.
 *
 * @SearchApiDataType(
 *   id = "rpt_jts",
 *   label = @Translation("RPT supporting multipolygons"),
 *   description = @Translation("Location data type implementation")
 * )
 */
class RptJTSDataType extends DataTypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue($value) {
    $file = File::load($value);
    $data = NULL;
    if ($file) {
      $data = file_get_contents($file->getFileUri());
      $geom = \geoPHP::load($data, 'json');
      if ($geom) {
        return $geom->out('wkt');
      }
      else {
        return $value;
      }
    }
  }
}
