<?php

namespace Drupal\leaflet_geojson_file\Plugin\search_api\data_type;

use Drupal\file\Entity\File;
use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides the location data type.
 *
 * @SearchApiDataType(
 *   id = "rpt_json",
 *   label = @Translation("RPT GeoJson"),
 *   description = @Translation("Location data type implementation")
 * )
 */
class RptJsonDataType extends DataTypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue($value) {
    $file = File::load($value);
    $data = NULL;
    if ($file) {
      $data = file_get_contents($file->getFileUri());
      $geom = \geoPHP::load($data);
      if ($geom) {
        return $geom->out('json');
      }
      else {
        return $value;
      }
    }
  }

}
