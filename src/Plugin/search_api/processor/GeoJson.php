<?php

namespace Drupal\leaflet_geojson_file\Plugin\search_api\processor;

use Drupal\file\Entity\File;
use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Add date ranges to the index.
 *
 * @SearchApiProcessor(
 *   id = "solr_geo_geojson",
 *   label = @Translation("GeoJSON data"),
 *   description = @Translation("Process GeoJson data."),
 *   stages = {
 *     "preprocess_index" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class GeoJson extends ProcessorPluginBase {

  /**
   * Process GeoJSON data from file and prepare a geometry collection.
   * Multiple features are merged into a single geometry collection.
   *
   * @param array $items
   */
  public function preprocessIndexItems(array $items) {
    foreach ($items as $item) {
      /** @var \Drupal\search_api\Item\FieldInterface $field */
      foreach ($item->getFields() as $field) {
        /** @var \Drupal\search_api\Entity\Index $index */
        $index = $field->getIndex();
        $field_settings = $index->getField($field->getFieldIdentifier());
        if ('rpt_json' === $field_settings->getType()) {

          $geometry_collection = [
            "type" => "GeometryCollection",
            "geometries" => [],
          ];

          $geometry_collection = [];

          foreach ($field->getValues() as $fid) {
            $file = File::load($fid);
            $data = NULL;

            if ($file) {
              $data = file_get_contents($file->getFileUri());
              $json = json_decode($data, TRUE);

              if (!empty($json['features'])) {
                foreach ($json['features'] as $feature) {
                  $geometry_collection[] = $feature['geometry'];
                }
              }
            }
          }
          $parsed_geometries_collection[] = json_encode($geometry_collection);
          $field->setValues($parsed_geometries_collection);
        }
      }
    }
  }

}
